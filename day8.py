#!/usr/bin/python3
from adventofcode import *
image = [l.strip() for l in open("input8", "r")][0]

WIDTH = 25
HEIGHT = 6
LAYERS = len(image)//(WIDTH*HEIGHT)

layers = [[image[i+l*WIDTH*HEIGHT] for i in range(WIDTH*HEIGHT)] for l in range(LAYERS)]
count = [[len(list(filter(lambda x: int(x)== c, l))) for l in layers] for c in range(3)]
minZ = min(count[0])
minZi = 0
while minZ != count[0][minZi]:
    minZi += 1
print("Part 1:", count[1][minZi]*count[2][minZi])

for p in range(WIDTH*HEIGHT):
    for l in range(LAYERS):
        if layers[0][p] == '2':
            layers[0][p] = layers[l][p]
        else:
            break

blackPixels = []    
for i in range(WIDTH*HEIGHT):
    if layers[0][i] in ['0', '2']:
        blackPixels.append((i%WIDTH, i//WIDTH))

from PIL import Image, ImageDraw
img = Image.new('RGB', (WIDTH, HEIGHT), color = (255, 255, 255))
 
d = ImageDraw.Draw(img)
d.point(blackPixels, (0,0,0))
img.save('day8.png')
print("Part 2: Image saved to day8.png")