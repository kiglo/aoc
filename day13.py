#!/usr/bin/python3
from adventofcode import *

computer = IntcodeComputer('input13')
output = computer.cleanRun()
width, height = [output[-3]+1, output[-2]+1]
image = [output[i:i+3] for i in range(0, len(output), 3)]

print("Part 1:", len(list(filter(lambda x: 1 if x[2] == 2 else 0, image))))

pixels = [[image[i*width+j][2] for j in range(width)] for i in range(height)]

animation = False
if animation:
    TILE_SIZE = 20
    import pygame
    pygame.init()
    screen=pygame.display.set_mode([width*TILE_SIZE, height*TILE_SIZE])
    screen.fill((0,0,0))


def getColor(p):
    if p == 0:
        return (0,0,0)
    if p == 1:
        return (100, 100, 100)
    if p == 2:
        return (0, 100, 0)
    if p == 3:
        return (140, 0, 0)
    return (255, 255, 255)

computer.programCode[0] = 2
computer.output = []
computer.pauseOnOutput = True
joystick = 0
paddlePos = 0
ballPos = 0
ballDir = 0
score = 0
while 1:
    if animation:
        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit()
    if ballPos > paddlePos:
        joystick = 1
        computer.input = []
    elif ballPos < paddlePos:
        joystick = -1
        computer.input = []
    else:
        joystick = 0
        computer.input = []
    for i in range(3):
        computer.output = []
        if len(computer.input) == 0:
            computer.input.extend([joystick]*5)
        output.extend(computer.resume())
    if len(output) < 3:
        break
    if output[0] != -1:
        if output[2] == 4:
            ballDir = output[0] - ballPos
            ballPos = output[0]
        if output[2] == 3:
            paddlePos = output[0]
        pixels[output[1]][output[0]] = output[2]
    else:
        score = output[2]
    output = []

    if animation:
        for i in range(width):
            for j in range(height):
                pygame.draw.rect(screen, getColor(pixels[j][i]), (i*TILE_SIZE, j*TILE_SIZE,TILE_SIZE, TILE_SIZE))
        pygame.display.flip()

if animation:
    pygame.quit()
print("Part 2:", score)