#!/usr/bin/python3
from adventofcode import *

computer = IntcodeComputer("input7")
maxOut = 0
for inputs in itertools.permutations([k for k in range(5)]):
    out = 0
    for i in inputs:
        out = computer.cleanRun([i, out])[0]
    if maxOut < out:
        maxOut = out
print("Part 1:", maxOut)

maxOut = 0
for inputs in itertools.permutations([k+5 for k in range(5)]):
    out = 0
    computers = [IntcodeComputer("input7") for i in range(5)]
    for i in range(5):
        computers[i].pauseOnOutput = True
        out = computers[i].cleanRun([inputs[i], out])[0]
    while computers[4].isRunning():
        for c in computers:
            c.input.append(out)
            out = c.resume()[-1]
    if maxOut < out:
        maxOut = out
print("Part 2:", maxOut)

