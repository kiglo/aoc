#!/usr/bin/python3
from adventofcode import *


color = {}

def move(pos, dir):
    if dir == 0:
        return (pos[0], pos[1]-1)
    if dir == 1:
        return (pos[0]+1, pos[1])
    if dir == 2:
        return (pos[0], pos[1]+1)
    if dir == 3:
        return (pos[0]-1, pos[1])

def run(computer, inputColor):
    #0 - up, 1 - right, 2 - down, 3 - left
    robotDir = 0
    position = (0, 0)
    global color
    computer.pauseOnOutput = True
    computer.cleanRun([inputColor])
    out = computer.resume()
    while computer.isRunning():
        color[position] = out[0]
        robotDir = (robotDir + out[1]*2-1)%4
        position = move(position, robotDir)
        computer.output = []
        c = 0
        if position in color.keys():
            c = color[position]
        computer.input.append(c)
        out = computer.resume()
        if computer.isRunning():
            out = computer.resume()

computer = IntcodeComputer('input11')
run(computer, 0)

print("Part 1:", len(color))

color = {}
run(computer, 1)
blackPixels = []
minX, maxX, minY, maxY = 0,0,0,0
for c in color:
    if c[0] < minX:
        minX = c[0]
    if c[0] > maxX:
        maxX = c[0]
    if c[1] < minY:
        minY = c[1]
    if c[1] > maxY:
        maxY = c[1]
    if color[c] == 1:
        blackPixels.append(list(c))
for i in range(len(blackPixels)):
    blackPixels[i][0] -= minY
    blackPixels[i][1] -= minX    
    blackPixels[i] = (blackPixels[i][0], blackPixels[i][1])    


from PIL import Image, ImageDraw
img = Image.new('RGB', (maxX-minX+1, maxY-minY+1), color = (255, 255, 255))
 
d = ImageDraw.Draw(img)
d.point(blackPixels, (0,0,0))
img.save('day11.png')
print("Part 2: Image saved to day11.png")
