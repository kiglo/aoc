#!/usr/bin/python3
from adventofcode import *
min_, max_ = [map(int, l.split('-')) for l in open("input4", "r")][0]

def digits(n):
    return [(n//10**i)%(10) for i in range(5,-1,-1)]

def check(n, partTwo = False):
    double = False
    maxTwo = False
    inc = True
    digit = digits(n)
    for i in range(1, len(digit)):
        if digit[i] == digit[i-1]:
            double = True
            if partTwo and (i > 1 and digit[i-2] != digit[i] or i == 1) and (i < len(digit)-1 and digit[i] != digit[i+1] or i == len(digit)-1):
                maxTwo = True
        if digit[i] < digit[i-1]:
            return False
    return double and (partTwo == maxTwo)

print("Part 1:", sum([check(i) for i in range(min_,max_+1)]))
print("Part 2:", sum([check(i, True) for i in range(min_,max_+1)]))