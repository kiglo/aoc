#!/usr/bin/python3
from adventofcode import *
data = [l.strip().split('=>') for l in open("input14", "r")]

class Chemical:
    def __init__(self, q, name=''):
        if type(q) == str:
            self.q, self.name = q.strip().split(' ')
            self.q = int(self.q)
        else:
            self.q = q
            self.name = name
    def __str__(self):
        return str(self.q) + " " + self.name
    
requirements = {}
produced = {}
used = 0

for d in data:
    c = Chemical(d[1])
    requirements[c.name] = [c.q, [Chemical(a) for a in d[0].split(',')]]
    produced[c.name] = 0

def produce(chem):
    global requirements
    global used
    if chem.name == "ORE":
        used += chem.q
        return chem.q
    r = requirements[chem.name]
    quantity = max(chem.q - produced[chem.name], 0)
    mul = (quantity-1)//r[0] + 1
    for c in r[1]:
        produce(Chemical(c.q*mul, c.name))
        if c.name != "ORE":
            produced[c.name] -= c.q*mul
    produced[chem.name] += r[0]*mul
    

produce(Chemical(1, "FUEL"))
print("Part 1:", used)
#binary search to find the maximum fuel
top, bottom = [1e12, 1]
while top > bottom+1:
    used = 0
    l = (top+bottom)//2
    for c in produced:
        produced[c] = 0
    produce(Chemical(l, "FUEL"))
    if used > 1e12:
        top = l
    else:
        bottom = l
print("Part 2:", int(bottom))