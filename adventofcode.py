import sys
import copy
import math
import itertools
import re
import time

class Point:
    def __init__(self, x, y, d = 0):
        self.x = x
        self.y = y
        self.d = d
    def __add__(self, p):
        return Point(self.x + p.x, self.y + p.y)
    def __sub__(self, p):
        return Point(self.x - p.x, self.y - p.y)
    def __eq__(self, p):
        if type(p) != type(self): return False
        return self.x == p.x and self.y == p.y and self.d == p.d
    def __str__(self):
        return "".join(["[",str(self.x), ", ", str(self.y), ", ", str(self.d),"]"])
    def length(self):
        return (self.x*self.x + self.y*self.y)**0.5
    def manhattan(self):
        return abs(self.x)+abs(self.y)
    def inside(self, bx, by, tx, ty):
        return self.x >= bx and self.x <= tx and self.y >= by and self.y <= ty

class IntcodeComputer:
    def __init__(self, filename):
        self.programCode = list([map(int, l.split(',')) for l in open(filename, "r")][0])
        self.output = []
        self.program = self.programCode[:]
        self.outputReadPosition = 0
        self.pauseOnOutput = False
        self.running = False
        self.base = 0

    def cleanRun(self, inputData = []):  
        """Runs the code using the original values
        
        Arguments:
            inputData {list} -- The input values used by read command, int values only
            reuseInput {bool} -- If true input values are not discarded after reading, after the last input is read, the reading continues from the first one
        
        Returns:
            list -- List of output values
        """
        self.base = 0
        self.output = []
        self.program = self.programCode[:]
        return self.run(inputData)

    def run(self, inputData = []):
        """Runs the code using the original values
        
        Arguments:
            inputData {list} -- The input values used by read command, int values only
            reuseInput {bool} -- If true input values are not discarded after reading, after the last input is read, the reading continues from the first one
        
        Returns:
            list -- List of output values
        """
        self.init(inputData)
        return self.resume()
       
    
    def init(self, inputData = []):
        self.i = 0
        self.input = inputData[:]
        self.halt = False
        self.running = True

    def pause(self):
        self.halt = True

    def resume(self, inputData = []):
        if not self.running:
            return self.cleanRun(inputData)
        self.input.extend(inputData)
        self.halt = False
        while not self.halt and self.program[self.i] != 99:
            self.i += self.execute(self.i, 1)
        self.running = self.program[self.i] != 99
        return self.output[:]

    def isRunning(self):
        return self.running

    def mode(self, code):
        opcode = code % 100
        modes = [(code//10**i)%(10) for i in range(2,5)]
        return [opcode, modes]

    def getVal(self, i, mode):
        if mode == 2:
            i += self.base
        if i >= len(self.program):
            self.program.extend([0] * (i+1-len(self.program)))
        return self.program[i]

    def setVal(self, i, value, mode):
        if mode == 2:
            i += self.base
        if i >= len(self.program):
            self.program.extend([0] * (i+1-len(self.program)))
        self.program[i] = value

    def read(self, i, mode):
        if mode == 0:
            return self.getVal(self.getVal(i, mode), mode)
        elif mode == 1:
            return self.getVal(i, 1)
        return self.getVal(self.getVal(i, 0), mode)
    
    def readInput(self):
        if len(self.input) == 0:
            print("Input error: no input in queue")
            self.halt = True
            return 0
        i = self.input[0]
        self.input.pop(0)
        if type(i) == int:
            return i
        print("Input error: invalid input", i)
        self.halt = True
        return 0

    def execute(self, c, id):
        modes = self.mode(self.program[c])
        if modes[0] == 1:
            self.setVal(self.getVal(c+3, 0), self.read(c+1, modes[1][0]) + self.read(c+2, modes[1][1]),modes[1][2])
        elif modes[0] == 2:
            self.setVal(self.getVal(c+3, 0), self.read(c+1, modes[1][0]) * self.read(c+2, modes[1][1]),modes[1][2])
        elif modes[0] == 3:
            self.setVal(self.getVal(c+1, 0), self.readInput(), modes[1][0])
        elif modes[0] == 4:
            self.output.append(self.read(c+1, modes[1][0]))
            if self.pauseOnOutput:
                self.pause()
        elif modes[0] == 5:
            if self.read(c+1, modes[1][0]) != 0:
                return self.read(c+2, modes[1][1]) - c
            else:
                return 3
        elif modes[0] == 6:
            if self.read(c+1, modes[1][0]) == 0:
                return self.read(c+2, modes[1][1]) - c
            else:
                return 3
        elif modes[0] == 7:
            if self.read(c+1, modes[1][0]) < self.read(c+2, modes[1][1]):
                self.setVal(self.getVal(c+3, 0), 1, modes[1][2])
            else:
                self.setVal(self.getVal(c+3, 0), 0, modes[1][2])
        elif modes[0] == 8:
            if self.read(c+1, modes[1][0]) == self.read(c+2, modes[1][1]):
                self.setVal(self.getVal(c+3, 0), 1, modes[1][2])
            else:
                self.setVal(self.getVal(c+3, 0), 0, modes[1][2])
        elif modes[0] == 9:
            baseshift = self.read(c+1, modes[1][0])
            self.base += baseshift
            return 2

        if modes[0] <= 2 or modes[0] >= 7:
            return 4
        return 2

def add_list(list_, value):
    if type(value) == list or type(value) == tuple:
        if len(list_) != len(value):
            raise Exception('List lengths differ')
        return [p[0]+p[1] for p in zip(list_, value)]
    if type(value) == int or type(value) == str:
        return [p+value for p in list_]

    
def mul_list(list_, value):
    if type(value) == list or type(value) == tuple:
        if len(list_) != len(value):
            raise Exception('List lengths differ')
        return [p[0]*p[1] for p in zip(list_, value)]
    if type(value) == int or type(value) == str:
        return [p*value for p in list_]
    
def getDir(d):
    if d == 1 or d == '^':
        return Point(0, -1)
    if d == 2 or d == 'v':
        return Point(0, 1)
    if d == 3 or d == '<':
        return Point(-1, 0)
    if d == 4 or d == '>':
        return Point(1, 0)

def getDirCmd(dir):
    if dir.y == -1:
        return 1
    if dir.y == 1:
        return 2
    if dir.x == -1:
        return 3
    if dir.x == 1:
        return 4

def reverseDir(d):      #1->2, 2->1, 3->4, 4->3
    return (d%2)*2-1 + d
def turnLeft(d):        #1->3, 2->4, 3->2, 4->1
    if d < 3: return d+2
    if d ==3: return 2
    return 1
def turnRight(d):
    return(turnLeft(turnLeft(turnLeft(d))))