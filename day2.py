#!/usr/bin/python3
from adventofcode import *

computer = IntcodeComputer("input2")
computer.programCode[1] = 12
computer.programCode[2] = 2
computer.cleanRun([])
print("Part 1:", computer.program[0])
i = 0
while computer.program[0] != 19690720:
    i += 1
    computer.programCode[1] = i // 99
    computer.programCode[2] = i % 99
    computer.cleanRun([])
print("Part 2:", 100*(i//99)+(i%99))