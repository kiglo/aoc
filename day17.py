#!/usr/bin/python3
from adventofcode import *

def makeFullPath(image, start):
    d = getDirCmd(getDir(image[start.y][start.x])) #convert char direction to 1-4
    pos = start
    instructions = []
    over = False
    while not over:
        forward = 0
        nextPos = pos+getDir(d)
        while nextPos.inside(0, 0, len(image[0])-1, len(image)-1): #move forward as long as possible
            if image[nextPos.y][nextPos.x] in ['#', 'O']:
                forward += 1
                nextPos += getDir(d)
            else:
                break
        nextPos -= getDir(d)    #take the last step back
        if forward > 0:
            instructions.append(str(forward))
        pos = nextPos
        d = turnLeft(d)         #trying to turn left
        turn = "L"
        nextPos = pos + getDir(d)
        if not nextPos.inside(0, 0, len(image[0])-1, len(image)-1) or image[nextPos.y][nextPos.x] not in ['#', 'O']:
            d = reverseDir(d)   #if not possible, turn right
            nextPos = pos + getDir(d)
            turn = "R"
            if not nextPos.inside(0, 0, len(image[0])-1, len(image)-1) or image[nextPos.y][nextPos.x] not in ['#', 'O']:
                over = True     #else, it is the end
        if not over:
            # image[pos.y][pos.x] = 'O'
            instructions.append(turn)
        
    return ','.join(instructions)

def toAscii(i):
    return bytes([i]).decode('ascii')
def toByte(c):
    return int(bytes(c, 'ascii'))

computer = IntcodeComputer("input17")
output = computer.cleanRun()

s = 0
startPos = Point(0, 0)
image = ''.join([toAscii(a) for a in output]).strip().split('\n')
for i in range(len(image)):
    for j in range(len(image[i])):
        if image[i][j] == '#' and i > 0 and j > 0 and i < len(image)-1 and j < len(image[i])-1:
            if image[i][j+1] == image[i][j-1] == image[i+1][j] == image[i-1][j] == '#':
                s += i*j
        if image[i][j] in ['^', '<', 'v', '>']:
            startPos = Point(j, i)
print("Part 1:", s)
# print('\n'.join(image))
path = makeFullPath([[c for c in row] for row in image], startPos)

mainRoutine = ''
fns = [''] * 3
for a in range(1, 21):
    for b in range(1, 21):
        for c in range(1, 21):
            # count = ''.join(path).count(''.join(A))
            tmp = path[:]
            A = tmp[:a]
            if A[-1] == ',':
                A = A[:-1]
            tmp = tmp.replace(A, "A")
            count = tmp.count(A)
            B = ""
            for startB in range(len(tmp)-b):
                if tmp[startB:startB+b].find("A") == -1 and tmp[startB] != ',':
                    B = tmp[startB:startB+b]
                    break
            if B == "":
                break
            if B[-1] == ',':
                B = B[:-1]
            tmp = tmp.replace(B, "B")
            C = ""
            for startC in range(len(tmp)-c):
                if tmp[startC:startC+c].find("A") == -1 and tmp[startC:startC+c].find("B") == -1 and tmp[startC] != ',':
                    C = tmp[startC:startC+c]
                    break
            if C == "":
                continue
            if C[-1] == ',':
                C = C[:-1]
            tmp = tmp.replace(C, "C")
            if len(tmp.replace("A", "").replace("B", "").replace("C", "").replace(",", "")) == 0:
                mainRoutine = tmp
                fns = [A, B, C]

computer.programCode[0] = 2
inputs = list(mainRoutine[:]+'\n')
inputs.extend(list('\n'.join(fns)+'\n'))
inputs = list(map(ord, inputs))
inputs.extend([ord('n'), ord('\n')])
print("Part 2:", computer.cleanRun(inputs)[-1])
