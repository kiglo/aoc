#!/usr/bin/python3
from adventofcode import *
positions = [re.findall('-?[\d]+', l.strip()) for l in open("input12", "r")]
positions = [[int(t) for t in p] for p in positions]
velocities = [[0,0,0] for p in positions]

def updateVel(positions, velocities):
    for i in range(len(positions)):
        for j in range(i+1, len(positions)):
            diff = add_list(positions[i], mul_list(positions[j], -1))
            diff = [0 if d == 0 else d//abs(d) for d in diff]
            velocities[i] = add_list(velocities[i], mul_list(diff, -1))
            velocities[j] = add_list(velocities[j], diff)
    
def updatePos(positions, velocities):
    for i in range(len(positions)):
        positions[i] = add_list(positions[i], velocities[i])

startPos = positions[:]
i = 0
c = 0
first = [False, False, False]
while c < 3 or i < 1000:
    updateVel(positions, velocities)
    updatePos(positions, velocities)
    i += 1

    if i == 1000:
        print("Part 1:", sum([sum([abs(p) for p in positions[i]])*sum([abs(v) for v in velocities[i]]) for i in range(len(positions))]))
    
    if i < 2:
        continue
    for k in range(3):
        if not first[k]:
            atStart = True
            for p in range(4):
                if positions[p][k] != startPos[p][k] or velocities[p][k] != 0:
                    atStart = False
            if atStart:
                c += 1
                first[k] = i
tick = first[0] // math.gcd(first[0], first[1]) * first[1]
tick = tick // math.gcd(tick, first[2]) * first[2]
print("Part 2:", tick)
   