#!/usr/bin/python3

data = [l.strip() for l in open("input1", "r")]
print("Part 1:",sum([int(n)//3-2 for n in data]))

total = 0
while len(data) > 0:
    data = list(filter(None, [max((0, int(n)//3-2)) for n in data]))
    total += sum(data)
print("Part 2:", total)