#!/usr/bin/python3
from adventofcode import *
data = [list(l.strip()) for l in open("input10", "r")]

def asteroid(x, y, xsize, ysize, data):
    if y < 0 or x < 0 or y > len(data)-1 or x > len(data[0])-1:
        return 0
    if data[y][x] == '#':
        return 1
    return asteroid(x+xsize, y+ysize, xsize, ysize, data)

def count(x,y, data):
    if data[y][x] == '.':
        return -1
    c = 0
    for dx in range(-x, len(data[0])-x):
        for dy in range(-y, len(data)-y):
            if dx == 0 and dy == 0 or math.gcd(dx, dy) != 1:
                continue
            c += asteroid(x+dx, y+dy, dx, dy, data)
            
    return c

detected = [[count(x, y, data) for x in range(len(data[y]))] for y in range(len(data))]
maxD = [0,0]
for i in range(len(detected)):
    for j in range(len(detected[i])):
        if detected[i][j] > detected[maxD[0]][maxD[1]]:
            maxD = [i,j]
print("Part 1:", detected[maxD[0]][maxD[1]])

y,x = maxD
asteroids = [[],[],[],[]]
for dx in range(0, len(data[0])-x):
    for dy in range(-1, -y-1, -1):
        if data[y+dy][x+dx] == '#':
            asteroids[0].append([x+dx, y+dy])
for dx in range(1, len(data[0])-x):
    for dy in range(0, len(data)-y):
        if data[y+dy][x+dx] == '#':
            asteroids[1].append([x+dx, y+dy])
for dx in range(0, -x-1, -1):
    for dy in range(1, len(data)-y):
        if data[y+dy][x+dx] == '#':
            asteroids[2].append([x+dx, y+dy])
for dx in range(-1, -x-1, -1):
    for dy in range(0, -y-1, -1):
        if data[y+dy][x+dx] == '#':
            asteroids[3].append([x+dx, y+dy])


removeIndex = 0
asteroids[0].sort(key=lambda a: (a[1]-y)/(a[0]-x+0.0001))
i = 0
while i < len(asteroids[0]):
    if i < len(asteroids[0])-1:
        a1, a2 = asteroids[0][i], asteroids[0][i+1]
        if abs((a1[1]-y)/(a1[0]-x+0.0001) - (a2[1]-y)/(a2[0]-x+0.0001)) < 0.001 or a1[0] == a2[0] and a1[0] == x:
            i += 1
            continue
    removeIndex += 1
    if removeIndex == 200:
        print("Part 2:", asteroids[0][i][0]*100+asteroids[0][i][1])
    asteroids[0].remove(asteroids[0][i])

asteroids[1].sort(key=lambda a: abs(a[1]-y)/abs(a[0]-x))
i = 0
k = 1
while i < len(asteroids[1]):
    if i < len(asteroids[1])-k:
        a1, a2 = asteroids[1][i], asteroids[1][i+k]
        if abs(abs(a1[1]-y)/abs(a1[0]-x) - abs(a2[1]-y)/abs(a2[0]-x)) < 0.0001:
            k += 1
            continue
    removeIndex += 1
    if removeIndex == 200:
        print("Part 2:", asteroids[1][i][0]*100+asteroids[1][i][1])
    asteroids[1].remove(asteroids[1][i])
    i += k-1
    k = 1

asteroids[2].sort(key=lambda a: abs(a[1]-y)/abs(a[0]-x+0.0001), reverse=True)
i = 0
k = 1
while i < len(asteroids[2]):
    if i < len(asteroids[2])-1:
        a1, a2 = asteroids[2][i], asteroids[2][i+1]
        if abs(abs(a1[1]-y)/abs(a1[0]-x+0.0001) - abs(a2[1]-y)/abs(a2[0]-x+0.0001)) < 0.0001 or a1[0]==a2[0] and a1[0] == x:
            i += 1
            continue
    removeIndex += 1
    if removeIndex == 200:
        print("Part 2:", asteroids[2][i][0]*100+asteroids[2][i][1])
    asteroids[2].remove(asteroids[2][i])
    # i += k-1
    # k = 1

asteroids[3].sort(key=lambda a: abs(a[1]-y)/abs(a[0]-x))
i = 0
k = 1
while i < len(asteroids[3]):
    if i < len(asteroids[3])-k:
        a1, a2 = asteroids[3][i], asteroids[3][i+k]
        if abs(abs(a1[1]-y)/abs(a1[0]-x) - abs(a2[1]-y)/abs(a2[0]-x)) < 0.0001:
            k += 1
            continue
    removeIndex += 1
    if removeIndex == 200:
        print("Part 2:", asteroids[3][i][0]*100+asteroids[3][i][1])
    asteroids[3].remove(asteroids[3][i])
    i += k-1
    k = 1