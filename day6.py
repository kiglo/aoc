#!/usr/bin/python3
from adventofcode import *
data = [l.strip().split(')') for l in open("input6", "r")]

m = {}
def orbits(name, _map, m):
    # global m
    if name in m:
        return m[name]
    around = list(filter((lambda i: i[1] == name), _map))
    if len(around) == 0:
        m[name] = [0, '']
    else:
        m[name] = [orbits(around[0][0], _map, m)[0] + 1, around[0][0]]
    return m[name]

def path(name, m):
    p = []
    while m[name][0] != 0:
        name = m[name][1]
        p.append(name)
    p.reverse()
    return p


print("Part 1:", sum([orbits(n[1], data, m)[0] for n in data]))
p1 = path("YOU", m)
p2 = path("SAN", m)
n = 0
while p1[n] == p2[n]:
    n += 1
print("Part 2:", len(p1)+len(p2)-2*n)