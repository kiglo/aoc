#!/usr/bin/python3
from adventofcode import *
datastr = [l.strip() for l in open("input16", "r")][0]
# datastr = '02935109699940807407585447034323'
data = [int(a) for a in datastr]

def runFFT(data):
    pattern = [0, 1, 0, -1]
    for i in range(100):
        tmp = []
        for c in range(len(data)):
            k = 0
            t = 1
            plist = []
            for j in range(len(data)):
                if t > c:
                    k = (k+1)%4
                    t = 0
                plist.append(pattern[k])
                t += 1
            tmp.append(abs(sum([plist[j]*data[j] for j in range(c, len(data))]))%10)
        data = tmp[:]
    return data


data = runFFT(data)
print("Part 1:", ''.join([str(d) for d in data[:8]]))

offset = int(datastr[:7])
data = [int(a) for a in (datastr*10000)[offset:]]

for p in range(100):
    for i in range(len(data)-2, -1, -1):
        data[i] = (data[i] + data[i+1]) % 10
    
print("Part 2:", ''.join([str(d) for d in data[:8]]))