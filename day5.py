#!/usr/bin/python3
from adventofcode import *

computer = IntcodeComputer("input5")
print("Part 1:", computer.cleanRun([1])[-1])
print("Part 2:", computer.cleanRun([5])[-1])