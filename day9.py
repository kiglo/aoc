#!/usr/bin/python3
from adventofcode import *

computer = IntcodeComputer("input9")
print("Part 1:", computer.cleanRun([1]))
print("Part 2:", computer.cleanRun([2]))