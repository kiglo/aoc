#!/usr/bin/python3
from adventofcode import *

computer = IntcodeComputer("input19")
print("Part 1:", sum([sum([computer.cleanRun([x, y])[-1] for y in range(50)]) for x in range(50)]))
def findFirst(x, y, minV, maxV):
    global computer
    if x < 0:
        for i in range(minV, maxV):
            if computer.cleanRun([i, y])[-1] > 0:
                return i
    else:
        for i in range(minV, maxV):
            if computer.cleanRun([x, i])[-1] > 0:
                return i
    return 0
def findLast(x, y, minV, maxV):
    global computer
    if x < 0:
        for i in range(minV, maxV):
            if computer.cleanRun([i, y])[-1] == 0:
                return i-1
    else:
        for i in range(minV, maxV):
            if computer.cleanRun([x, i])[-1] == 0:
                return i-1
    return 0

MAX_VALUE = 10000

def getValue(x, y):
    global computer
    return computer.cleanRun([x, y])[-1]

size = 100
y = size + 5
x = findLast(-1, y, findFirst(-1, y, 0, MAX_VALUE), MAX_VALUE)
while getValue(x, y) != 1 or getValue(x-size+1, y+size-1) != 1:
    y += 1
    x = findLast(-1, y, x, MAX_VALUE)
x -= size-1
print("Part 2:", x * 10000 + y)