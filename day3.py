#!/usr/bin/python3
from adventofcode import *
data = [[n for n in l.strip().split(',')] for l in open("input3", "r")]
points = [[],[]]

def newpoint(p, dir):
    d = [0,0]
    if dir[0] == 'U':
        d = [0, 1]
    if dir[0] == 'D':
        d = [0, -1]
    if dir[0] == 'R':
        d = [1, 0]
    if dir[0] == 'L':
        d = [-1, 0]
    return [p[0] + d[0]*int(dir[1:]), p[1]+d[1]*int(dir[1:]), p[2] + int(dir[1:])]

def between(a, b, c):
    if a > b:
        a,b = b,a
    return c > a and c < b

def intersect(p1, p2, q1, q2):
    if p1[0] == p2[0] and q1[0] == q2[0] or p1[1] == p2[1] and q1[1] == q2[1]:
        return False
    if p1[0] == p2[0] and between(q1[0],q2[0],p1[0]) and between(p1[1],p2[1],q1[1]):
        return [p1[0],q1[1],p1[2]+q1[2]+abs(q1[1]-p1[1])+abs(p1[0]-q1[0])]
    elif p1[1] == p2[1] and between(p1[0],p2[0],q1[0]) and between(q1[1],q2[1],p1[1]):
        return [p1[1],q1[0],p1[2]+q1[2]+abs(q1[0]-p1[0])+abs(p1[1]-q1[1])]
    return False

p = [[0,0,0],[0,0,0]]
for k in range(2):
    points[k].append(p[k])
    for i in range(len(data[k])):
        p[k] = newpoint(p[k], data[k][i])
        points[k].append(p[k])

minD = -1
minSteps = -1
for i in range(len(points[0])-1):
    for j in range(len(points[1])-1):
        m = intersect(points[0][i],points[0][i+1],points[1][j],points[1][j+1])
        if m:
            if minD == -1 or abs(m[0])+abs(m[1]) < minD:
                minD = abs(m[0])+abs(m[1])
            if minSteps == -1 or m[2] < minSteps:
                minSteps = m[2]

print("Part 1:", minD)
print("Part 2:", minSteps)