#!/usr/bin/python3
from adventofcode import *

computer = IntcodeComputer("input15")
computer.pauseOnOutput = True
map_ = {}
minC = Point(0,0)
maxC = Point(0,0)

i = 0
d = 3
pos = Point(0,0)
map_[str(Point(0, 0))] = 1
oxygenPos = 0
while i < 10000 or oxygenPos == 0:
    pos += getDir(d)
    if str(pos) not in map_.keys():
        map_[str(pos)] = computer.resume([d])[-1]
    if map_[str(pos)] == 2:
        oxygenPos = pos
    if pos.x < minC.x: minC.x = pos.x
    if pos.y < minC.y: minC.y = pos.y
    if pos.x > maxC.x: maxC.x = pos.x
    if pos.y > maxC.y: maxC.y = pos.y
    d = turnLeft(d)
    map_[str(pos+getDir(d))] = computer.resume([d])[-1]
    while map_[str(pos+getDir(d))] == 0:
        d = turnRight(d)
        map_[str(pos+getDir(d))] = computer.resume([d])[-1]
    i += 1

maze = [[Point(0, (maxC-minC).x * (maxC-minC).y * 2) for i in range(maxC.x-minC.x+3)] for j in range(maxC.y-minC.y+3)]
for i in range(len(maze)):
    # print("")
    for j in range(len(maze[i])):
        if str(Point(j + minC.x-1, i + minC.y-1)) in map_.keys():
            maze[i][j].x = map_[str(Point(j+ minC.x-1, i + minC.y-1))]
        # if maze[i][j].x == 0: print('██', end='')
        # elif maze[i][j].x == 1: print('  ', end='')
        # else: print('()', end='')

w = len(maze[0])
h = len(maze)
oxygenPos -= minC - Point(1, 1)
startPos = Point(1, 1)-minC
maze[oxygenPos.y][oxygenPos.x].y = 0
queue = [oxygenPos]
maxDist = 0
while len(queue) > 0:
    p = queue.pop(0)
    if maze[p.y][p.x].x == 0: continue
    d = maze[p.y][p.x].y
    if d > maxDist: maxDist = d
    if p.x > 0 and maze[p.y][p.x-1].y > d + 1: 
        queue.append(Point(p.x-1, p.y))
        maze[p.y][p.x-1].y = d+1
    if p.y > 0 and maze[p.y-1][p.x].y > d + 1: 
        queue.append(Point(p.x, p.y-1))
        maze[p.y-1][p.x].y = d+1
    if p.x < w-1 and maze[p.y][p.x+1].y > d + 1: 
        queue.append(Point(p.x+1, p.y))
        maze[p.y][p.x+1].y = d+1
    if p.y < h-1 and maze[p.y+1][p.x].y > d + 1: 
        queue.append(Point(p.x, p.y+1))
        maze[p.y+1][p.x].y = d+1

print("Part 1:", maze[startPos.y][startPos.x].y)
print("Part 2:", maxDist)